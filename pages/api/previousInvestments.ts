import { NextApiRequest, NextApiResponse } from 'next';
import { getPreviousInvestments } from '@/handlers/previousInvestmentsHandler';

export default async (request: NextApiRequest, response: NextApiResponse) => {
  const { clientNo, sortBy, offset, limit } = request.query;
  console.log('API fired')
  response.writeHead(200, { 'Content-Type': 'application/json' });
  response.write(
    JSON.stringify(
      await getPreviousInvestments(
        (clientNo || 1300107) as any,
        (sortBy || 'code.asc') as any,
        (offset || 0) as any,
        (limit || 10) as any,
      ),
    ),
  );
  response.end();
};
