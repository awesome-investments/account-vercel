import React from 'react';
import { usePreviousInvestments } from '@/api/apiConfig';
import { DisposalTypeDict, PreviousInvestment } from '@/api/apiSchema';
import { translateToPageUnit } from '@/components/ui/Pagination/paginationUtils';
import withPaginatedFetcher, { UsePaginatedFetchData } from '@/components/ui/Pagination/withPaginatedFetcher';
import BaseLink from '@/ui/link/BaseLink';
import Table, { TableBody, TableCell, TableHeader, TableHeaderData, TableRow } from '@/ui/Table';
import { formatDate } from '@/utils/dateFormat';
import { prop } from '@/utils/objectUtils';

type PreviousInvestmentsTableProps = {
  data: PreviousInvestment[];
};

export const PreviousInvestmentsTable = ({ data }: PreviousInvestmentsTableProps) => {
  const tableHeaders: TableHeaderData[] = [
    { value: 'Code' },
    { value: 'Investment' },
    { value: 'Disposal type' },
    { value: 'Disposal date' },
  ];

  return (
    <>
      <Table className="min-w-md md:min-w-full mb-8 md:mb-10 lg:mb-12" noDataMessage="No previous investments to show.">
        <TableHeader headers={tableHeaders} />
        <TableBody>
          {data.map(({ id, sedol, name, description, disposalType, disposalDate }, index) => (
            <TableRow key={id} rowIndex={index}>
              <TableCell>{sedol}</TableCell>
              <TableCell>
                <BaseLink
                  link={`http://google.com?q=${sedol}`}
                  className="text-sm underline hover:no-underline"
                  a11yPrefix="Investment history for"
                >
                  {`${name} ${description}`}
                </BaseLink>
              </TableCell>
              <TableCell>{prop(disposalType, DisposalTypeDict)}</TableCell>
              <TableCell>{formatDate(disposalDate)}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>{' '}
    </>
  );
};

export type SortByItem = {
  label: string;
  value: string;
};

type PreviousInvestmentDataProps = {
  sortBy: SortByItem;
};

const usePreviousInvestmentFetcher: UsePaginatedFetchData<PreviousInvestmentDataProps, PreviousInvestmentsTableProps> =
  ({ sortBy, pagination }) => {
    const { previousInvestments, error } = usePreviousInvestments(pagination, sortBy.value);
    const { currentPage } = translateToPageUnit(pagination);
    return {
      componentProps: previousInvestments ? { data: previousInvestments.data } : undefined,
      pagination: previousInvestments ? previousInvestments.pagination : pagination,
      announcerMessage: previousInvestments
        ? `Previous investment page ${currentPage} data loaded and sorted by ${sortBy.label}`
        : `Previous investment page ${currentPage} data is being loaded`,
      error,
    };
  };

export default withPaginatedFetcher(usePreviousInvestmentFetcher, PreviousInvestmentsTable);
