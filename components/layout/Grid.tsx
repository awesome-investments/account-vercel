import classNames from 'classnames';
import { ReactNode } from 'react';

type GridProps = {
  sm?: number;
  md?: number;
  lg?: number;
  smGap?: number;
  mdGap?: number;
  lgGap?: number;
  className?: string;
  children: ReactNode;
};

const Grid = ({ sm, md, lg, smGap = 5, mdGap = 5, lgGap = 7.5, className, children }: GridProps) => {
  const classes = classNames(
    { [`grid grid-cols-${sm} gap-${smGap}`]: sm },
    { [`md:grid md:grid-cols-${md} md:gap-${mdGap} lg:gap-${lgGap}`]: md },
    { [`lg:grid lg:grid-cols-${lg} lg:gap-${lgGap}`]: lg },
    className,
  );

  return <div className={`${classes}`}>{children}</div>;
};

export default Grid;
