import { mocked } from 'ts-jest/utils';
import { useIntersectionObserver } from '@/hooks/useIntersectionObserver';

jest.mock('@/hooks/useIntersectionObserver');
mocked(useIntersectionObserver).mockImplementation(() => false);
