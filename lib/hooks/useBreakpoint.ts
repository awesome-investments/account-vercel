import { useEffect, useState } from 'react';
import { useWindowWidth } from './useWindowWidth';

// eslint-disable-next-line no-shadow
export enum Breakpoint {
  sm = 'sm',
  md = 'md',
  lg = 'lg',
}

export const useBreakpoint = (): Breakpoint => {
  const [breakpoint, setBreakpoint] = useState(Breakpoint.sm);
  const width = useWindowWidth();

  useEffect(() => {
    if (width < 768) {
      setBreakpoint(Breakpoint.sm);
    } else if (width < 1024) {
      setBreakpoint(Breakpoint.md);
    } else {
      setBreakpoint(Breakpoint.lg);
    }
  }, [width]);

  return breakpoint;
};
