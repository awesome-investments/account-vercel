import { getClientNo } from '@/api/./clientNoConfig';
import { PaginationData, PreviousInvestments } from '@/api/apiSchema';
import { useApi } from '@/api/useApi';
import { fetcher } from './fetcher';

export const usePreviousInvestments = ({ offset, limit }: PaginationData, sortBy: string) => {
  const getPreviousInvestmentsUrl = () => {
    const apiUrl = process.env.NEXT_PUBLIC_API_URL as string;
    const previousInvestmentsEndpoint = process.env.NEXT_PUBLIC_PREVIOUS_INVESTMENTS_ENDPOINT as string;
    return previousInvestmentsEndpoint;
  };

  const { data: previousInvestments, error } = useApi<PreviousInvestments>(
    [offset, limit, sortBy],
    (offsetQuery, limitQuery, sortByQuery) =>
      fetcher(getPreviousInvestmentsUrl(), {
        offset: offsetQuery,
        limit: limitQuery,
        sortBy: sortByQuery,
      }),
  );

  return { previousInvestments, error };
};
