import axios from 'axios';

export const fetcher = (url: string, params = {}) =>
  axios
    .get(url, {
      params,
    })
    .then((result) => result.data);
