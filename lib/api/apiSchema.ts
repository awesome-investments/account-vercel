export type PaginationData = {
  offset: number;
  limit: number;
  total: number;
};

export enum DisposalTypeDict {
  CONVERSION_OUT = 'Conversion out',
  CORPORATE_ACTION = 'Corporate action',
  SOLD = 'Sold',
  TRANSFER_OUT = 'Transfer out',
  LODGEMENT = 'Lodgement',
  WITHDRAWAL = 'Withdrawal',
}

export type PreviousInvestment = {
  id: string;
  sedol: string;
  name: string;
  description: string;
  disposalType: keyof typeof DisposalTypeDict;
  disposalDate: string;
};

export type PreviousInvestments = {
  pagination: PaginationData;
  data: PreviousInvestment[];
};

export type Period = {
  period: number;
  periodUnit: 'day' | 'month' | 'taxYear';
};
