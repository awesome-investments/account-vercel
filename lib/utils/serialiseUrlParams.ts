type serialiseUrlParamsType = {
  [key: string]: string | number;
};

const serialiseUrlParams = (obj: serialiseUrlParamsType) => {
  const str = `?${Object.entries(obj)
    .map(([key, val]) => `${key}=${encodeURIComponent(val)}`)
    .join('&')}`;
  return str;
};

export default serialiseUrlParams;
