export type NumberFormatConfigType = {
  minimumFractionDigits?: number;
  maximumFractionDigits?: number;
};

const defaultNumberFormatConfig: NumberFormatConfigType = {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
};

export const absoluteNumberFormat = (val: number, config?: NumberFormatConfigType) =>
  numberFormat(Math.abs(val), config);

export const numberFormat = (val: number, config?: NumberFormatConfigType) => {
  const numberConfig = {
    ...defaultNumberFormatConfig,
    ...config,
  };

  return val.toLocaleString('en-GB', numberConfig);
};
